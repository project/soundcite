/**
 * @file registers the Soundcite toolbar button and binds functionality to it.
 */

import { Plugin } from 'ckeditor5/src/core';
import { ButtonView, ContextualBalloon, clickOutsideHandler } from 'ckeditor5/src/ui';
import icon from '../../../../icons/soundcite.svg';
import FormView from './soundciteview';
import { ClickObserver } from 'ckeditor5/src/engine';

export default class SoundciteUI extends Plugin {
  static get requires() {
    return [ ContextualBalloon ];
  }
  init() {
    const editor = this.editor;
    this._balloon = this.editor.plugins.get( ContextualBalloon );
    this.formView = this._createFormView();

    // This will register the Soundcite toolbar button.
    editor.ui.componentFactory.add('Soundcite', (locale) => {
      const command = editor.commands.get('insertSoundcite');
      const buttonView = new ButtonView(locale);

      // Create the toolbar button.
      buttonView.set({
        label: editor.t('Soundcite'),
        icon,
        tooltip: true,
      });

      // Bind the state of the button to the command.
      buttonView.bind('isOn', 'isEnabled').to(command, 'value', 'isEnabled');

      // Execute the command when the button is clicked (executed).
      this.listenTo(buttonView, 'execute', () =>
        // editor.execute('insertSoundcite'),
        this._showUI()
      );

      return buttonView;
    });

    editor.editing.view.addObserver( ClickObserver );
    editor.listenTo( editor.editing.view.document, 'click', ( evt, data ) => {
      const modelElement = editor.editing.mapper.toModelElement( data.target);

      if ( modelElement.name == 'Soundcite' ) {
        this.formView.url.fieldView.value = modelElement.getAttribute('data-url');
        this.formView.start.fieldView.value = modelElement.getAttribute('data-start');
        this.formView.end.fieldView.value = modelElement.getAttribute('data-end');
        this.formView.plays.fieldView.value = modelElement.getAttribute('data-plays');
        this.formView.text.fieldView.value = modelElement.getAttribute('text');
        this._showUI();
      }
    } );
  }
  _createFormView() {
    const editor = this.editor;
    const formView = new FormView( editor.locale );
    // Execute the command after clicking the "Save" button.
    this.listenTo( formView, 'submit', () => {
      // Grab values from input fields.
      const value = {
			  url: formView.url.fieldView.element.value,
				start: formView.start.fieldView.element.value,
        end: formView.end.fieldView.element.value,
        plays: formView.plays.fieldView.element.value,
        text: formView.text.fieldView.element.value
			};
      editor.execute( 'insertSoundcite', value );
      // Hide the form view after submit.
      this._hideUI();
    });
    // Hide the form view after clicking the "Cancel" button.
    this.listenTo( formView, 'cancel', () => {
      this._hideUI();
    });
    // Hide the form view when clicking outside the balloon.
    clickOutsideHandler( {
      emitter: formView,
      activator: () => this._balloon.visibleView === formView,
      contextElements: [ this._balloon.view.element ],
      callback: () => this._hideUI()
    });
    return formView;
  }
  _showUI() {
    this._balloon.add( {
      view: this.formView,
      position: this._getBalloonPositionData()
    });
    this.formView.focus();
  }
  _hideUI() {
    // Clear the input field values and reset the form.
    this.formView.url.fieldView.value = '';
    this.formView.start.fieldView.value = '';
    this.formView.end.fieldView.value = '';
    this.formView.plays.fieldView.value = '';
    this.formView.text.fieldView.value = '';
    this.formView.element.reset();
    this._balloon.remove( this.formView );
    // Focus the editing view after inserting the content so the user can start typing the content
    // right away and keep the editor focused.
    this.editor.editing.view.focus();
  }
  _getBalloonPositionData() {
    const view = this.editor.editing.view;
    const viewDocument = view.document;
    let target = null;
    // Set a target position by converting view selection range to DOM
    target = () => view.domConverter.viewRangeToDom( viewDocument.selection.getFirstRange() );
    return {
      target
    };
  }
}
