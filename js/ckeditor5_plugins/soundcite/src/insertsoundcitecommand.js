/**
 * @file defines InsertSoundciteCommand, which is executed when the Soundcite
 * toolbar button is pressed.
 */
// cSpell:ignore soundciteediting

import { Command } from 'ckeditor5/src/core';

export default class InsertSoundciteCommand extends Command {
  execute(attributes) {
    const { model } = this.editor;

    model.change((writer) => {
      // Insert <Soundcite>*</Soundcite> at the current selection position
      // in a way that will result in creating a valid model structure.

      model.insertContent(createSoundcite(writer,attributes));
    });
  }

  refresh() {
    const { model } = this.editor;
    const { selection } = model.document;

    // Determine if the cursor (selection) is in a position where adding a
    // Soundcite is permitted. This is based on the schema of the model(s)
    // currently containing the cursor.
    const allowedIn = model.schema.findAllowedParent(
      selection.getFirstPosition(),
      'Soundcite',
    );

    // If the cursor is not in a location where a Soundcite can be added, return
    // null so the addition doesn't happen.
    this.isEnabled = allowedIn !== null;
  }
}

function createSoundcite(writer,attributes) {
  // Create instances of the three elements registered with the editor in
  // soundciteediting.js.
  // console.log(attributes);
  const Soundcite = writer.createElement('Soundcite',{
    'data-url': attributes.url,
    'data-start': attributes.start ? attributes.start : 0,
    'data-end': attributes.end ? attributes.end : null,
    'data-plays': attributes.plays ? attributes.plays : 1,
    'text': attributes.text
  });

  // Return the element to be added to the editor.
  return Soundcite;
}
