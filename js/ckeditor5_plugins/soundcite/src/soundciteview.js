import {
  View,
  LabeledFieldView,
  createLabeledInputText,
  ButtonView,
  submitHandler
} from 'ckeditor5/src/ui';
import { icons } from 'ckeditor5/src/core';

export default class FormView extends View {
  constructor(locale) {
    super(locale);
    this.url = this._createInput('Url');
    this.start = this._createInput('Start');
    this.end = this._createInput('End');
    this.plays = this._createInput('Plays');
    this.text = this._createInput('Link Text');
    this.saveButtonView = this._createButton('Save', icons.check, 'ck-button-save');
    this.saveButtonView.type = 'submit';
    this.cancelButtonView = this._createButton('Cancel', icons.cancel, 'ck-button-cancel');
    this.cancelButtonView.delegate('execute').to(this, 'cancel');
    this.childViews = this.createCollection([
      this.url,
      this.start,
      this.end,
      this.plays,
      this.text,
      this.saveButtonView,
      this.cancelButtonView
    ]);
    this.setTemplate({
      tag: 'form',
      attributes: {
        class: ['ck', 'ck-abbr-Soundcite'],
        tabindex: '-1'
      },
      children: this.childViews
    });
  }
  render() {
    super.render();
    submitHandler({
      view: this
    });
  }
  focus() {
    this.childViews.first.focus();
  }

  _createInput(label) {
    const labeledInput = new LabeledFieldView(this.locale, createLabeledInputText);
    labeledInput.label = label;
    return labeledInput;
  }
  _createButton(label, icon, className) {
    const button = new ButtonView();
    button.set({
      label,
      icon,
      tooltip: true,
      class: className
    });
    return button;
  }
}
