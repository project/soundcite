<?php

namespace Drupal\soundcite\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class Settings. The config form for the soundcite module.
 *
 * @package Drupal\soundcite\Form
 */
class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'soundcite_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'soundcite.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('soundcite.settings');

    $form['soundcloud_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Soundcloud Client ID'),
      '#description' => $this->t(' If you use SoundCloud, you may want to register for a client ID. If you don’t, your clips may occasionally become unplayable because of the amount of traffic against the SoundciteJS default SoundCloud client ID. See the <a href="https://developers.soundcloud.com/docs/api/guide">SoundCloud Developers website</a> to register for a client ID.'),
      '#default_value' => $config->get('soundcloud_client_id'),
    ];

    $form['background_color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Background Color'),
      '#description' => $this->t(' If you choose, you may set the background color of your clips.'),
      '#default_value' => $config->get('background_color'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable('soundcite.settings')
      // Set the submitted configuration setting.
      ->set('soundcloud_client_id', $form_state->getValue('soundcloud_client_id'))
      ->set('background_color', $form_state->getValue('background_color'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
